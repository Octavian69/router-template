import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemPageComponent } from './system-page/system-page.component';
import { SystemGuard } from '../shared/guards/system.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    { path: '', component: SystemPageComponent, canActivate: [SystemGuard], children: [
        { path: 'home', component: HomeComponent }
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystemRoutingModule {}