import { NgModule } from '@angular/core';
import { SystemPageComponent } from './system-page/system-page.component';
import { SystemGuard } from '../shared/guards/system.guard';
import { SharedModule } from '../shared/modules/shared.module';
import { SystemRoutingModule } from './system.routing.module';
import { HomeComponent } from './home/home.component';

@NgModule({
    declarations: [
        SystemPageComponent,
        HomeComponent
    ],
    imports: [
        SharedModule,
        SystemRoutingModule
    ],
    providers: [SystemGuard]
})
export class SystemModule {}