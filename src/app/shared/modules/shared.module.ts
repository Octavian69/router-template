import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const declarations = [];

const imports = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
];

const exports = [
    ...declarations,
    ...imports
];

@NgModule({
    declarations,
    imports,
    exports
})
export class SharedModule {}