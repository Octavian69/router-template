import { Injectable } from '@angular/core';
import { CoreModule } from '../modules/core.module';
import { Router } from '@angular/router';

@Injectable({
    providedIn: CoreModule
})
export class AuthService {

    isLogged: boolean = false;

    constructor(
        private router: Router
    ) {}

    getLogged(): boolean {
        return this.isLogged;
    }

    setLogged(flag: boolean): void {
        if(this.isLogged !== flag) {
            this.isLogged = flag;
        }
    }

    login(): void {
        this.setLogged(true);
        this.router.navigateByUrl('/home');
    }

    logout(): void {
        this.setLogged(false);
        this.router.navigateByUrl('/login');
    }
}