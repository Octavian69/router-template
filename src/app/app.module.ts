import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { AuthModule } from './auth-module/auth.module';
import { SystemModule } from './system-module/system.module';
import { SharedModule } from './shared/modules/shared.module';

import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { CoreModule } from './shared/modules/core.module';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    CoreModule.forRoot(),
    SharedModule,
    AuthModule,
    SystemModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
