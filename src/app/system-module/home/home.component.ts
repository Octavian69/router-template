import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'rt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isOut: boolean = false;
  

  constructor(
    public authService: AuthService,
  ) { }

  ngOnInit(): void {
  }

}
