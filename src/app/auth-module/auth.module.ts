import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/modules/shared.module';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { LoginComponent } from './login/login.component';
import { RegistartionComponent } from './registartion/registartion.component';
import { AuthRoutingModule } from './auth.routing.module';
import { AuthGuard } from '../shared/guards/auth.guard';

@NgModule({
    declarations: [
        AuthPageComponent, 
        LoginComponent, 
        RegistartionComponent
    ],
    imports: [
        SharedModule,
        AuthRoutingModule
    ],
    providers: [
        AuthGuard
    ]
})
export class AuthModule {}